import React from 'react'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { SignupScreen, LoginScreen } from '../screens/auth-screens'

const Tab = createStackNavigator()

export const RootNavigation = () => {

    return (
        <NavigationContainer >
            <Tab.Navigator screenOptions = {{headerShown: false}}>
                <Tab.Screen name={'sign-up-screen'} component={SignupScreen} />
                <Tab.Screen name={'login-screen'} component={LoginScreen} />
            </Tab.Navigator>
        </NavigationContainer>
    )
}