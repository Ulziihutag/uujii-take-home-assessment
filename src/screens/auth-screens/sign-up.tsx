import React, { useState, useContext } from 'react'
import { StyleSheet, TextInput, TouchableOpacity, Image, SafeAreaView, Modal, View } from 'react-native'
import { Box, Spacing, Stack } from '../../components/layout'
import { Text, Border } from '../../components/core'
import { useNavigation } from '@react-navigation/native';
import { useCollection } from '../../hooks'
import { AuthContext } from '../../provider'
import auth from '@react-native-firebase/auth';

export const SignupScreen = () => {
    const [phoneNumber, setPhoneNumber] = useState('')
    const [name, setName] = useState('')
    const [confirm, setConfirm] = useState(null)
    const [code, setCode] = useState('')
    const [modal, setModal] = useState(false)
    const navigation = useNavigation()
    const { createUser } = useCollection('users')
    const { user } = useContext(AuthContext);

    const createNewUser = async () => {
        await setModal(true)
        const confirmation: any = await auth().signInWithPhoneNumber(`+976 ${phoneNumber}`)
        console.log('confirmation', confirmation)
        await setConfirm(confirmation)

    }
    const confirmCode = async () => {
        try {
            await setModal(false)
            await confirm?.confirm(code);
            await createUser(user && user.uid, name)
        } catch (error) {
            console.log('Invalid code.');
        }
    }
    console.log(phoneNumber)
    return (
        <SafeAreaView>
            <Modal animationType='slide' transparent={true} visible={modal} onRequestClose={() => setModal(false)}>
                <View style={styles.modal}>
                    <View style={styles.modalView}>
                        <Spacing pl={5} pr={5}>
                            <Stack size={4}>
                                <Text>Confirmation Code</Text>
                                <Border lineWidth={1} radius={10} color={'#50C2C9'}>
                                    <Box width={'100%'} height={50} backgroundColor={'#FFFFFF'}>
                                        <Spacing pl={4}>
                                            <TextInput placeholder={'Phone Number'} style={styles.textInput} onChangeText={(number) => setCode(number)} defaultValue={code} />
                                        </Spacing>
                                    </Box>
                                </Border>
                                <Spacing pt={10}>
                                    <TouchableOpacity onPress={() => confirmCode()}>
                                        <Box width={'100%'} backgroundColor={'#50C2C9'} height={56} justifyContent={'center'} alignItems={'center'}>
                                            <Text color={'white'} bold={true} fontSize={25}>Send</Text>
                                        </Box>
                                    </TouchableOpacity>
                                </Spacing>
                            </Stack>
                        </Spacing>
                    </View>
                </View>
            </Modal>
            <Box justifyContent={'center'} width={'100%'} height={'100%'}>
                <Image style={styles.img} source={require('../../assets/img/Shape.png')} />
                <Spacing pl={5} pr={5}>
                    <Box flexDirection={'column'} width={'100%'}>
                        <Stack size={10}>
                            <Box>
                                <Stack size={3}>
                                    <Text>Phone Number</Text>
                                    <Border radius={20}>
                                        <Box width={'100%'} height={50} backgroundColor={'#FFFFFF'}>
                                            <Spacing pl={4}>
                                                <TextInput placeholder={'Phone Number'} style={styles.textInput} onChangeText={(number) => setPhoneNumber(number)} defaultValue={phoneNumber} />
                                            </Spacing>
                                        </Box>
                                    </Border>
                                </Stack>
                            </Box>
                            <Box>
                                <Stack size={3}>
                                    <Text>Name</Text>
                                    <Border radius={20}>
                                        <Box width={'100%'} height={50} backgroundColor={'#FFFFFF'}>
                                            <Spacing pl={4}>
                                                <TextInput placeholder={'Name'} style={styles.textInput} onChangeText={(text) => setName(text)} defaultValue={name} />
                                            </Spacing>
                                        </Box>
                                    </Border>
                                </Stack>
                            </Box>
                            <Box>
                                <Spacing pb={3}>
                                    <TouchableOpacity onPress={() => navigation.navigate('login-screen')}>
                                        <Text color={'#50C2C9'}>Already have an account?</Text>
                                    </TouchableOpacity>
                                </Spacing>
                                <TouchableOpacity onPress={() => createNewUser()}>
                                    <Box width={'100%'} backgroundColor={'#50C2C9'} height={56} justifyContent={'center'} alignItems={'center'}>
                                        <Text color={'white'} bold={true} fontSize={25}>Register</Text>
                                    </Box>
                                </TouchableOpacity>
                            </Box>
                        </Stack>
                    </Box>
                </Spacing>
            </Box>
        </SafeAreaView>
    )
}
const styles = StyleSheet.create({
    img: {
        position: 'absolute',
        width: 150,
        height: 150,
        top: -50,
        left: 0
    },
    textInput: {
        width: '100%',
        height: '100%'
    },
    modal: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },
    modalView: {
        width: '80%',
        height: 400,
        justifyContent: 'center',
        backgroundColor: "white",
        borderRadius: 20,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5
    },
})