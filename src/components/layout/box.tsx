import React from 'react'
import { View, StyleSheet } from 'react-native'

type BoxType = {
    flex?: number;
    flexDirection?: 'row' | 'row-reverse' | 'column' | 'column-reverse';
    flexWrap?: 'nowrap' | 'wrap' | 'wrap-reverse';
    justifyContent?: 'flex-start' | 'flex-end' | 'center' | 'space-between' | 'space-around' | 'space-evenly';
    alignItems?: 'stretch' | 'flex-start' | 'flex-end' | 'center' | 'baseline';
    alignSelf?: 'stretch' | 'flex-start' | 'flex-end' | 'center';
    position?: 'absolute' | 'relative';
    top?: string | number;
    bottom?: string | number;
    left?: string | number;
    right?: string | number;
    width?: string | number;
    height?: string | number;
    backgroundColor?: string;
    children?: JSX.Element | JSX.Element[] | string | any;
}

export const Box: React.FC<BoxType> = (props) => {
    const {
        flex = 0,
        height,
        width,
        flexDirection,
        flexWrap,
        justifyContent,
        alignItems,
        alignSelf,
        position,
        children,
        top,
        bottom,
        left,
        right,
        backgroundColor
    } = props;

    const styles = StyleSheet.create({
        box: {
            flex,
            width,
            height,
            backgroundColor: backgroundColor ? backgroundColor : 'transparent'
        },
        position: {
            position: position || 'relative',
            top,
            bottom,
            left,
            right,
        },
        flexBox: {
            display: 'flex',
            flexDirection: flexDirection || 'column',
            justifyContent: justifyContent || 'flex-start',
            alignItems: alignItems || 'stretch',
            alignSelf: alignSelf,
        }
    })

    return (
        <View
            style={[
                styles.box,
                styles.flexBox,
                styles.position,
            ]}
        >
            {children}
        </View>
    )
}