import React from 'react'
import {StyleSheet, Text as RNText} from 'react-native'

type TextType = {
    bold?: boolean;
    fontSize?: number;
    color?: string;
    children?: JSX.Element | JSX.Element[] | string;
}

export const Text: React.FC<TextType> = (props) => {
    const {bold, fontSize, children, color} = props

    const styles = StyleSheet.create({
        text: {
            fontSize: fontSize ? fontSize : 17,
            fontWeight: bold ? '700' : '400',
            color: color ? color : 'black'
        }
    })
    return(
        <RNText style = {styles.text}>{children}</RNText>
    )
}