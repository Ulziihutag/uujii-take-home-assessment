import React from 'react';
import { StyleSheet, View } from 'react-native';

type BorderType = {
    radius?: number;
    lineWidth?: number;
    topWidth?: number;
    bottomWidth?: number;
    leftWidth?: number;
    rightWidth?: number;
    color?: string;
    children?: JSX.Element | JSX.Element[] | string;
};

export const Border: React.FC<BorderType> = (props) => {

    const {
        children,
        lineWidth,
        radius,
        topWidth,
        bottomWidth,
        leftWidth,
        rightWidth,
        color
        } = props

    const style = StyleSheet.create({
        border: {
            borderColor: color ? color : 'black',
            borderWidth: lineWidth,
            borderRadius: radius,
            borderTopWidth: topWidth,
            borderBottomWidth: bottomWidth,
            borderLeftWidth: leftWidth,
            borderRightWidth: rightWidth,
            overflow: 'hidden',
        },
    });
    return <View style={style.border}>{children}</View>;
};