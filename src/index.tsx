import React from 'react'
import {SafeAreaView} from 'react-native'
import {RootNavigation} from './navigation'

export const Main = () => {

    return(
        <RootNavigation/>
    )
}