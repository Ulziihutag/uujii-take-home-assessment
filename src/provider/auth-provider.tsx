import React, { createContext, useEffect, useState } from 'react';
import auth from '@react-native-firebase/auth';

export const AuthContext = createContext({
  user: null,
});

export const AuthUserProvider = ({ children }: any) => {
  const [user, setuser] = useState(null);
  useEffect(() => {
    auth().onAuthStateChanged(async (user) => {
      setuser(user);
      if (!user) {
        return;
      }
    });
  }, []);

  return (
    <AuthContext.Provider
      value={{
        user,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};
